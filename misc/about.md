+++
title = "Experience"
weight = 30
draft = false
+++

{{< figure class="image main" src="/images/arrisboxes.jpg" >}}
I’m a passionate sales professional with a strong business acumen. Equally comfortable negotiating deals as I am tackling the latest technology trends, my career in telecommunications is founded in technical skills and driven by a thirst for new challenges.

Account Director, Networks & Cloud, Canada West
ARRIS
Apr 2016 to present

Technical Account Manager, Networks & Cloud, Canada West
ARRIS
March 2014 to March 2016

Systems Analyst, IT Trading Systems
TransAlta
Jan 2014 – Mar 2014

Systems Engineer, Guide Applications 
Shaw Cablesystems
Jun 2011 – Dec 2013

Systems Architect, Video Systems Engineering 
Shaw Cablesystems
May 2010 – Jun 2011

Product Development Engineer
Advanced Flow Technologies Inc.
Dec 2011 – Apr 2012
Entrepreneurial Ventures

API Architect & Developer
FastCab
Dec 2011 - Apr 2012
Education

B.Sc. Electrical Engineering
University of Alberta
2003-2007


Skills & Accomplishments
ARRIS Century Club, 2016
ARRIS Customer CONNECT Winner, 2016
ARRIS Circle of Excellence, 2015
Excellent presentation skills
Effective communicator & problem solver
Motivated self-starter

Formal training in:

Negotiation skills
Strategic account planning
Challenger sales techniques